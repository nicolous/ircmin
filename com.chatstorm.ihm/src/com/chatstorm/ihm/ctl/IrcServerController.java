/**
 * 
 */
package com.chatstorm.ihm.ctl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.chatstorm.ihm.console.ClientCommand;
import com.chatstorm.ihm.events.ChatListener;
import com.ircmin.irc.client.ClientImpl;
import com.ircmin.irc.client.api.Client;
import com.ircmin.irc.network.api.ServerListener;
import com.ircmin.irc.protocol.api.CommandResponses;
import com.ircmin.irc.protocol.api.ErrorReplies;
import com.ircmin.irc.protocol.api.ProtocolHandler;
import com.ircmin.irc.protocol.api.ProtocolListener;

/**
 * @author Nicolas Maingot
 *
 */
public class IrcServerController implements ServerListener, ProtocolListener
{
    private final Client client;
    private final ChatListener listener;

    private String nickname;
    private final String password;
    private final InetSocketAddress server;

    private StringBuilder motd;
    private final Map<String, String[]> namelist;

    private final Map<String, ClientCommand> clientCommands;

    ClientOutput output;

    /**
     * 
     */
    public IrcServerController(
            final ClientOutput output,
            final InetSocketAddress server, final String password, final String nickname,
            final ChatListener listener)
                    throws UnknownHostException, IOException
                    {
        this.output = output;
        this.nickname = nickname;
        this.password = password;
        this.server = server;
        namelist = new HashMap<>();
        client = new ClientImpl(this.server);
        client.getConnection().addListener(this);
        client.getProtocolHandler().addListener(this);

        this.listener = listener;

        clientCommands = new HashMap<>();
        initClientCommands();

        client.connect();
                    }

    private void registerCommand(final ClientCommand command)
    {
        clientCommands.put(command.getName(), command);
    }

    private void initClientCommands()
    {
        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "nick";
            }

            @Override
            public String getDescription()
            {
                return "nick <nickname> - Set the nickname.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                client.getProtocolHandler().nick(args);
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "join";
            }

            @Override
            public String getDescription()
            {
                return "join <channel> - Join a channel.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                final String[] tokens = args.split(" ");
                final String newChannel;
                final String password;
                if (tokens.length >= 2)
                {
                    newChannel = tokens[1];
                    if (tokens.length == 3)
                        password = tokens[2];
                    else
                        password = null;
                    client.getProtocolHandler().join(
                            Collections.singletonMap(newChannel, password));
                }
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "part";
            }

            @Override
            public String getDescription()
            {
                return "part <channel> [reason] - Part a channel, with an optionnal reason.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                final String[] tokens = args.split(" ");
                if (tokens.length == 2)
                {
                    client.getProtocolHandler().part(tokens[1]);
                }
                else if (tokens.length == 3)
                {
                    client.getProtocolHandler().part(tokens[1], tokens[2]);
                }
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "quit";
            }

            @Override
            public String getDescription()
            {
                return "quit [reason] - Quit the server.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                final String[] tokens = args.split(" ");
                if (tokens.length == 1)
                {
                    client.getProtocolHandler().quit();
                }
                else
                {
                    client.getProtocolHandler().quit(tokens[1]);
                }
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "notice";
            }

            @Override
            public String getDescription()
            {
                return "notice <recipient> [message] - Send a notice to an user or a recipient.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                final String[] tokens = args.split(" ");
                if (tokens.length == 3)
                {
                    client.getProtocolHandler().notice(tokens[1], ":" + tokens[2]);
                }
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "row";
            }

            @Override
            public String getDescription()
            {
                return "row <command string> - Send a row command string to the server.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                try
                {
                    client.getConnection().send(
                            args.substring(getName().length() + 1) + ProtocolHandler.END_OF_CMD);
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                }
            }
        });

        registerCommand(new ClientCommand()
        {

            @Override
            public String getName()
            {
                return "help";
            }

            @Override
            public String getDescription()
            {
                return "help - Help.";
            }

            @Override
            public void execute(final String args) throws Exception
            {
                for (final Entry<String, ClientCommand> cmd : clientCommands.entrySet())
                {
                    output.message('\t' + cmd.getValue().getDescription());
                }
            }
        });
    }

    public boolean tryRun(final String input) throws Exception
    {
        boolean isCommand = false;

        if (input.startsWith("/"))
        {
            final String args = input.substring(1);
            final ClientCommand command = clientCommands.get(args.split(" ")[0]);
            if (command != null)
            {
                isCommand = true;
                command.execute(args);
            }
        }

        return isCommand;
    }

    public void join(final Map<String, String> channels) throws IOException
    {
        client.getProtocolHandler().join(channels);
    }

    public void nick(final String nickname) throws IOException
    {
        this.nickname = nickname;
        client.getProtocolHandler().nick(nickname);
    }

    public void privmsg(final String channel, final String message) throws IOException
    {
        client.getProtocolHandler().privmsg(channel, message);
    }

    public String getNickname()
    {
        return nickname;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPing(final String daemon)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReply(
            final String sender, final int code, final String recipient, final String message)
    {
        final String[] tokens = message.split(" ");

        if (code >= ErrorReplies.START && code <= ErrorReplies.END)
            listener.onError(getServerString(server), message);

        switch (code)
        {
        case ErrorReplies.ERR_NOMOTD:
            listener.onMotd(getServerString(server), null);
            break;

        case CommandResponses.RPL_MOTDSTART:
            motd = new StringBuilder(message);
            break;
        case CommandResponses.RPL_MOTD:
            if (motd != null)
                motd.append(message).append('\n');
            break;
        case CommandResponses.RPL_ENDOFMOTD:
            if (motd != null)
            {
                motd.append(message);
                listener.onMotd(getServerString(server), motd.toString());
            }
            motd = null;
            break;

        case CommandResponses.RPL_TOPIC:
            if (tokens.length >= 2)
            {
                listener.onTopic(
                        getServerString(server), tokens[0],
                        message.substring(tokens[0].length() + 1));
            }
            break;
        case CommandResponses.RPL_NOTOPIC:
            if (tokens.length >= 2)
            {
                listener.onTopic(getServerString(server), tokens[0], null);
            }
            break;

        case CommandResponses.RPL_NAMREPLY:
            if (tokens.length >= 2)
            {
                final String chanVisibility = tokens[0];
                final String channel = tokens[1];
                namelist.put(
                        channel,
                        message
                        .substring(chanVisibility.length() + 1 + channel.length() + 2)
                        .split(" "));
            }
            break;
        case CommandResponses.RPL_ENDOFNAMES:
            if (tokens.length >= 2)
            {
                final String channel = tokens[0];
                if (namelist.containsKey(channel))
                    listener.onUserList(getServerString(server), channel, namelist.get(channel));
            }
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNotice(final String sender, final String recipient, final String message)
    {
        listener.onTell(getServerString(server), sender, recipient, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPrivmsg(final String sender, final String recipient, final String message)
    {
        listener.onSay(getServerString(server), recipient, sender, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onMode(final String sender, final String nickname, final String mode)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onJoin(final String channel, final String sender)
    {
        listener.onJoin(getServerString(server), channel, sender);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPart(final String sender, final String channel, final String message)
    {
        listener.onPart(getServerString(server), channel, sender, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onQuit(final String sender, final String message)
    {
        listener.onQuit(getServerString(server), sender, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnect(final InetSocketAddress server)
    {
        listener.onConnect(getServerString(server));
        try
        {
            if (password != null)
            {
                client.getProtocolHandler().pass(password);
            }
            client.getProtocolHandler().nick(nickname);
            client.getProtocolHandler().user(nickname, nickname);
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisconnect(final InetSocketAddress server)
    {
        listener.onDisconnect(getServerString(server), "Connection closed.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(final String data)
    {
        listener.onReceive(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSend(final String data)
    {
        listener.onSend(data);
    }

    private static String getServerString(final InetSocketAddress server)
    {
        return server.getHostString() + ':' + server.getPort();
    }
}
