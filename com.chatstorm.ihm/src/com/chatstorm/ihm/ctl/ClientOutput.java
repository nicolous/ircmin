/**
 * 
 */
package com.chatstorm.ihm.ctl;

/**
 * @author Nicolas Maingot
 *
 */
public interface ClientOutput
{
    void message(String message);

    void error(String message);

    void logStdout(String message);

    void logSterr(String message);
}
