/**
 * 
 */
package com.chatstorm.ihm.events;

import java.util.List;

/**
 * @author Nicolas Maingot
 *
 */
public interface ChatListener
{
    void onConnect(String server);

    void onMotd(String server, String motd);

    void onUserMode(String server, List<String> modes);

    void onChanMode(String server, String channel, List<String> modes);

    void onJoin(String server, String channel, String user);

    void onTopic(String server, String channel, String topic);

    void onUserList(String server, String channel, String[] users);

    void onPart(String server, String channel, String user, String reason);

    void onQuit(String server, String user, String reason);

    void onKick(String server, String channel, String user, String message);

    void onBan(String server, String channel, String user, String message);

    void onError(String server, String message);

    void onDisconnect(String server, String reason);

    void onSay(String server, String recipient, String user, String message);

    void onTell(String server, String user, String recipient, String message);

    void onModeChange(String server, List<String> modes);

    void onReceive(String data);

    void onSend(String data);
}
