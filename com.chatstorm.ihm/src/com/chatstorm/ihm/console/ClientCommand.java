/**
 * 
 */
package com.chatstorm.ihm.console;

/**
 * @author Nicolas Maingot
 *
 */
public interface ClientCommand
{
    String getName();

    String getDescription();

    void execute(String args) throws Exception;

    // TODO: auth
}
