/**
 * 
 */
package com.chatstorm.ihm.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.chatstorm.ihm.ctl.ClientOutput;
import com.chatstorm.ihm.ctl.IrcServerController;
import com.chatstorm.ihm.events.ChatListener;

/**
 * @author Nicolas Maingot
 *
 */
public class IrcConsole implements ChatListener, ClientOutput
{
    private final IrcServerController controller;
    private final BufferedReader input;

    private final String channel;
    private final String chanPassword;

    /**
     * @throws IOException
     * @throws UnknownHostException
     * 
     */
    public IrcConsole(
            final InetSocketAddress server, final String servPassword,
            final String nickname, final String channel, final String chanPassword)
                    throws UnknownHostException, IOException
                    {
        this.channel = channel;
        this.chanPassword = chanPassword;
        input = new BufferedReader(new InputStreamReader(System.in));
        controller = new IrcServerController(this, server, servPassword, nickname, this);

        final Thread inputThread = new Thread()
        {
            /**
             * {@inheritDoc}
             */
            @Override
            public void run()
            {
                boolean eos = false;
                while (!eos)
                {
                    String text = null;
                    try
                    {
                        text = input.readLine();
                    }
                    catch (final IOException e)
                    {
                        e.printStackTrace();
                    }
                    if (text == null)
                        eos = true;
                    else
                    {
                        try
                        {
                            if (! controller.tryRun(text))
                            {
                                controller.privmsg(channel, text);
                                showPrivmsg(controller.getNickname(), text);
                            }
                        }
                        catch (final Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        inputThread.setDaemon(false);
        inputThread.start();
                    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnect(final String server)
    {
        showEvent("Connected to " + server + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onMotd(final String server, final String motd)
    {
        showEvent(motd);
        try
        {
            controller.join(Collections.singletonMap(channel, chanPassword));
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUserMode(final String server, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onChanMode(final String server, final String channel, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onJoin(final String server, final String channel, final String user)
    {
        final String nickname = getNickname(user);
        if (nickname.equals(controller.getNickname()))
        {
            showEvent("Joined " + channel + '.');
        }
        else
        {
            showEvent(nickname + " has joined the channel.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onTopic(final String server, final String channel, final String topic)
    {
        showEvent("Topic is " + topic + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUserList(final String server, final String channel, final String[] users)
    {
        final String memberlist;
        if (users.length == 0)
        {
            memberlist = "empty";
        }
        else
        {
            final StringBuilder buffer = new StringBuilder();
            for (final String member : users)
            {
                buffer.append(member).append(", ");
            }
            buffer.delete(buffer.length() - 2, buffer.length() - 1);
            memberlist = buffer.toString();
        }
        showEvent("User list: " + memberlist + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPart(
            final String server, final String channel, final String user, final String reason)
    {
        showEvent(getNickname(user) + " parts, reason: " + reason + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onQuit(final String server, final String user, final String reason)
    {
        showEvent(getNickname(user) + " quits, reason: " + reason + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onKick(
            final String server, final String channel, final String user, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBan(
            final String server, final String channel, final String user, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onError(final String server, final String message)
    {
        showEvent(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisconnect(final String server, final String reason)
    {
        showEvent("Disconnected from " + server + ", reason: " + reason + '.');
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSay(
            final String server, final String recipient, final String user, final String message)
    {
        if (recipient.equals(controller.getNickname()))
            showPrivmsgPrivate(user, message);
        else
            showPrivmsg(user, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onTell(
            final String server, final String user, final String recipient, final String message)
    {
        showNotice(user, recipient, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onModeChange(final String server, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(final String data)
    {
        // showEvent("<<< " + data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSend(final String data)
    {
        // showEvent(">>> " + data.replace("\r", "").replace("\n", ""));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void message(final String message)
    {
        System.out.println(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(final String message)
    {
        System.err.println(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logStdout(final String message)
    {
        System.out.println(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logSterr(final String message)
    {
        System.err.println(message);
    }

    private static String getDate()
    {
        final String date;

        final DateFormat format = new SimpleDateFormat("HH:mm");
        date = format.format(new Date());

        return date;
    }

    private static String getNickname(final String user)
    {
        final String nickname;

        final int nickSep = user.indexOf('!');
        if (nickSep == -1)
            nickname = user;
        else
            nickname = user.substring(0, nickSep);

        return nickname;
    }

    private void showPrivmsg(final String sender, final String text)
    {
        message("[" + getDate() + "] " + getNickname(sender) + ": " + text);
    }

    private void showPrivmsgPrivate(final String sender, final String text)
    {
        message("[" + getDate() + "] --> " + getNickname(sender) + ": " + text);
    }

    private void showNotice(final String sender, final String recipient, final String text)
    {
        message("["
                + getDate() + "] " + getNickname(sender) + "(to " + recipient + ") " + text);
    }

    private void showEvent(final String text)
    {
        message("[" + getDate() + "] " + text);
    }

    public static void main(final String[] args) throws UnknownHostException, IOException
    {
        if (args.length < 4)
        {
            System.err.println("Arguments: <hostname> <port> <nickname> <channel>");
        }
        else
        {
            final String hostname = args[0];
            int port = -1;
            try
            {
                port = Integer.parseInt(args[1]);
            }
            catch (final NumberFormatException e)
            {
                System.err.println("Invalid port.");
                System.exit(0);
            }
            final String nickname = args[2];

            final String channel = args[3];

            new IrcConsole(new InetSocketAddress(hostname, port), null, nickname, channel, null);
        }
    }
}
