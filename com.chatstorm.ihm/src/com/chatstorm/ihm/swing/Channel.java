/**
 * 
 */
package com.chatstorm.ihm.swing;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

/**
 * @author Nicolas Maingot
 *
 */
public class Channel extends JPanel
{
    private final JTextArea outputAreaText;

    /**
     * Create the panel.
     */
    public Channel()
    {
        setLayout(new BorderLayout(0, 0));

        final JList<String> userlist = new JList<String>();
        userlist.setBorder(new EmptyBorder(5, 5, 5, 5));

        final JScrollPane userlistScroll = new JScrollPane(userlist);
        add(userlistScroll, BorderLayout.WEST);

        final JPanel header = new JPanel();
        header.setBorder(new EmptyBorder(5, 5, 5, 5));
        add(header, BorderLayout.NORTH);
        header.setLayout(new BorderLayout(0, 0));

        final JPanel inputArea = new JPanel();
        inputArea.setBorder(new EmptyBorder(5, 5, 5, 5));
        add(inputArea, BorderLayout.SOUTH);
        inputArea.setLayout(new BorderLayout(0, 0));

        final JTextArea inputAreaText = new JTextArea();
        inputAreaText.setRows(1);
        inputAreaText.setLineWrap(true);
        inputAreaText.setWrapStyleWord(true);

        final JScrollPane inputTextAreaScroll = new JScrollPane(inputAreaText);
        inputArea.add(inputTextAreaScroll, BorderLayout.CENTER);

        final JButton btnSend = new JButton("Send");
        inputArea.add(btnSend, BorderLayout.EAST);

        outputAreaText = new JTextArea();
        outputAreaText.setEditable(false);
        outputAreaText.setLineWrap(true);
        outputAreaText.setWrapStyleWord(true);

        final JScrollPane outputAreaScroll = new JScrollPane(outputAreaText);
        outputAreaScroll
        .setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        add(outputAreaScroll, BorderLayout.CENTER);
    }

    public void addOutput(final String text)
    {
        outputAreaText.append(text + '\n');
    }
}
