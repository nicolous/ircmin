/**
 * 
 */
package com.chatstorm.ihm.swing;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

/**
 * @author Nicolas Maingot
 *
 */
public class IrcPanel extends JPanel
{

    private final JTabbedPane tabs;

    private final Map<String, List<Channel>> channels;

    /**
     * Create the panel.
     */
    public IrcPanel()
    {
        channels = new HashMap<>();

        setLayout(new BorderLayout(0, 0));

        tabs = new JTabbedPane(SwingConstants.TOP);
        add(tabs);
    }

    public void addChannel(final String server, final String chanName)
    {
        final Channel channel = new Channel();
        tabs.addTab(chanName, null, channel, null);

        List<Channel> servChan = channels.get(server);
        if (servChan == null)
        {
            servChan = new ArrayList<>();
            channels.put(server, servChan);
        }
        servChan.add(channel);
    }

    public void removeChannel(final String server, final String chanName)
    {
        Channel channel = null;
        for (int i = 0; i < tabs.getTabCount(); i++)
        {
            if (chanName.equals(tabs.getTitleAt(i)))
            {
                channel = (Channel) tabs.getComponentAt(i);
                tabs.removeTabAt(i);
                break;
            }
        }
        if (channel != null)
        {
            channels.get(server).remove(channel);
        }
    }

    public List<Channel> getChannels()
    {
        final List<Channel> channels = new ArrayList<>();
        for (int i = 0; i < tabs.getTabCount(); i++)
        {
            channels.add((Channel) tabs.getComponentAt(i));
        }
        return channels;
    }

    public Channel getChannel(final String server, final String chanName)
    {
        Channel channel = null;
        for (int i = 0; i < tabs.getTabCount(); i++)
        {
            if (chanName.equals(tabs.getTitleAt(i)))
            {
                channel = (Channel) tabs.getComponentAt(i);
                break;
            }
        }
        return channel;
    }
}
