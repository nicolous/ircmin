/**
 * 
 */
package com.chatstorm.ihm.swing.desktop;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.chatstorm.ihm.ctl.ClientOutput;
import com.chatstorm.ihm.ctl.IrcServerController;
import com.chatstorm.ihm.events.ChatListener;
import com.chatstorm.ihm.swing.Channel;
import com.chatstorm.ihm.swing.IrcPanel;

/**
 * @author Nicolas Maingot
 *
 */
public class ApplicationWindow implements ChatListener, ClientOutput
{
    private JFrame frmChatstorm;

    private IrcPanel ircPanel;

    private final Map<String, IrcServerController> controllers;

    /**
     * Launch the application.
     */
    public static void main(final String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                try
                {
                    final ApplicationWindow window = new ApplicationWindow();
                    window.frmChatstorm.setVisible(true);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ApplicationWindow()
    {
        controllers = new HashMap<>();

        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize()
    {
        frmChatstorm = new JFrame();
        frmChatstorm.setTitle("chatstorm");
        frmChatstorm.setBounds(100, 100, 450, 300);
        frmChatstorm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ircPanel = new IrcPanel();
        frmChatstorm.getContentPane().add(ircPanel, BorderLayout.CENTER);

        connect("irc.quakenet.org", 6667);
    }

    private void connect(final String hostname, final int port)
    {
        final String server = hostname + ':' + port;
        final IrcServerController controller;
        if (controllers.containsKey(server))
        {
            controller = controllers.get(server);
        }
        else
        {
            try
            {
                controller =
                        new IrcServerController(
                                this,
                                new InetSocketAddress(hostname, port), null, "chatstorm",
                                this);
                controllers.put(server, controller);
            }
            catch (final UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnect(final String server)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onMotd(final String server, final String motd)
    {
        try
        {
            controllers.get(server).join(Collections.singletonMap("#esfr-bot", (String) null));
            ircPanel.addChannel(server, "#esfr-bot");
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUserMode(final String server, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onChanMode(final String server, final String channel, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onJoin(final String server, final String channel, final String user)
    {
        final Channel tab = ircPanel.getChannel(server, channel);
        tab.addOutput(user + " joins the channel");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onTopic(final String server, final String channel, final String topic)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUserList(final String server, final String channel, final String[] users)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPart(
            final String server, final String channel, final String user, final String reason)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onQuit(final String server, final String user, final String reason)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onKick(
            final String server, final String channel, final String user, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBan(
            final String server, final String channel, final String user, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onError(final String server, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisconnect(final String server, final String reason)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSay(
            final String server, final String recipient, final String user, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onTell(
            final String server, final String user, final String recipient, final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onModeChange(final String server, final List<String> modes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(final String data)
    {
        System.out.println("<<< " + data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSend(final String data)
    {
        System.out.println(">>> " + data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void message(final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logStdout(final String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logSterr(final String message)
    {
    }
}
