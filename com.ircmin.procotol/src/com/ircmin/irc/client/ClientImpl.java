/**
 * 
 */
package com.ircmin.irc.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import com.ircmin.irc.client.api.Client;
import com.ircmin.irc.network.ConnectionImpl;
import com.ircmin.irc.network.api.Connection;
import com.ircmin.irc.protocol.ProtocolHandlerImpl;
import com.ircmin.irc.protocol.api.ProtocolHandler;

/**
 * @author Nicolas Maingot
 *
 */
public class ClientImpl implements Client
{
    private final ProtocolHandler ircHandler;

    private final Connection connection;

    /**
     * 
     * @param host
     * @param port
     * @throws UnknownHostException
     * @throws IOException
     */
    public ClientImpl(final InetSocketAddress server)
    {
        connection = new ConnectionImpl(server);
        ircHandler = new ProtocolHandlerImpl(connection);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connect() throws UnknownHostException, IOException
    {
        connection.connect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolHandler getProtocolHandler()
    {
        return ircHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection getConnection()
    {
        return connection;
    }

    // public static void main(final String[] args) throws Exception
    // {
    // final Client cl = new ClientImpl("irc.gamesurge.net", 6667);
    // cl.getConnection().addServerListener(new ServerListener()
    // {
    //
    // @Override
    // public void onReceive(final String data)
    // {
    // }
    //
    // @Override
    // public void onDisconnect(final String hostname, final int port)
    // {
    // System.out.println("disconnect");
    // }
    //
    // @Override
    // public void onConnect(final String hostname, final int port)
    // {
    // System.out.println("connect");
    // try
    // {
    // cl.getProtocolHandler().nick("Nicolous2_nick");
    // cl.getProtocolHandler().user("Nicolous2_user", "*", "*", "NM");
    // }
    // catch (final IOException e)
    // {
    // e.printStackTrace();
    // }
    // }
    // });
    //
    // cl.addIrcListener(new IrcListener()
    // {
    //
    // @Override
    // public void onReply(final String sender, final int code, final String message)
    // {
    // System.out.println("reply " + sender + " " + code + " " + message);
    //
    // if (code == CommandResponses.RPL_LUSERCLIENT)
    // {
    // try
    // {
    // cl.getProtocolHandler().join(
    // Collections.singletonMap("#esfr-bot", (String) null));
    // }
    // catch (final IOException e)
    // {
    // e.printStackTrace();
    // }
    // }
    // }
    //
    // @Override
    // public void onPrivmsg(final String sender, final String recipient, final String message)
    // {
    // System.out.println("privmsg " + sender + " " + recipient + " " + message);
    // }
    //
    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public void onMode(final String sender, final String nickname, final String mode)
    // {
    // System.out.println("mode " + sender + " " + nickname + " " + mode);
    // }
    //
    // @Override
    // public void onPing(final String daemon)
    // {
    // System.out.println("ping " + daemon);
    // }
    //
    // @Override
    // public void onPart(final String sender, final String channel, final String message)
    // {
    // System.out.println("part " + sender + " " + channel + " " + message);
    // }
    //
    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public void onQuit(final String sender, final String channel, final String message)
    // {
    // System.out.println("quit " + sender + " " + channel + " " + message);
    // }
    //
    // @Override
    // public void onNotice(final String sender, final String recipient, final String message)
    // {
    // System.out.println("notice " + sender + " " + recipient + " " + message);
    // }
    //
    // @Override
    // public void onJoin(final String sender, final String channel)
    // {
    // System.out.println("join " + sender + " " + channel);
    // }
    // });
    //
    // cl.connect();
    //
    // Thread.sleep(30000);
    // cl.getProtocolHandler().quit("Bye.");
    // }

}
