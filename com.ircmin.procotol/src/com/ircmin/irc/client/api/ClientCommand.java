/**
 * 
 */
package com.ircmin.irc.client.api;

/**
 * @author Nicolas Maingot
 *
 */
public interface ClientCommand
{
    void execute(String[] args);
}
