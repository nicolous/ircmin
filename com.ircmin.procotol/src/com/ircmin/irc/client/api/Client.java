/**
 * 
 */
package com.ircmin.irc.client.api;

import java.io.IOException;
import java.net.UnknownHostException;

import com.ircmin.irc.network.api.Connection;
import com.ircmin.irc.protocol.api.ProtocolHandler;

/**
 * @author Nicolas Maingot
 *
 */
public interface Client
{
    ProtocolHandler getProtocolHandler();

    Connection getConnection();

    void connect() throws UnknownHostException, IOException;
}
