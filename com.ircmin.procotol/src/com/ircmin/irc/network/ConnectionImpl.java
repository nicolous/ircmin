/**
 * 
 */
package com.ircmin.irc.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

import com.ircmin.irc.network.api.Connection;
import com.ircmin.irc.network.api.ServerListener;

/**
 * @author Nicolas Maingot
 *
 */
public class ConnectionImpl implements Connection
{

    private Socket socket;
    private BufferedReader input;
    private BufferedWriter output;

    private final InetSocketAddress server;

    private final Set<ServerListener> listeners;

    /**
     * 
     * @param host
     * @param port
     */
    public ConnectionImpl(final InetSocketAddress server)
    {
        this.server = server;
        listeners = new HashSet<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connect() throws UnknownHostException, IOException
    {
        if (socket == null)
        {
            socket = new Socket(server.getHostString(), server.getPort());
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            notifyConnect();

            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        while (! socket.isClosed())
                        {
                            final String data = input.readLine();
                            if (data == null)
                                disconnect();
                            else
                                notifyReceive(data);
                        }
                    }
                    catch (final IOException e)
                    {
                        e.printStackTrace();
                    }
                };
            }.start();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect() throws IOException
    {
        ensureConnected();
        try
        {
            socket.close();
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
        notifyDisconnect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InetSocketAddress getServerAddress()
    {
        return server;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPort()
    {
        return socket.getPort();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final String command) throws IOException
    {
        ensureConnected();
        output.write(command);
        output.flush();
        notifySend(command);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(final ServerListener listener)
    {
        synchronized (listeners)
        {
            listeners.add(listener);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeListener(final ServerListener listener)
    {
        synchronized (listeners)
        {
            listeners.remove(listener);
        }
    }

    private void ensureConnected() throws IOException
    {
        if (socket == null || socket.isClosed())
            throw new IOException("Not connected.");
    }

    private void notifyReceive(final String command)
    {
        synchronized (listeners)
        {
            for (final ServerListener listener : listeners)
            {
                try
                {
                    listener.onReceive(command);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void notifySend(final String command)
    {
        synchronized (listeners)
        {
            for (final ServerListener listener : listeners)
            {
                try
                {
                    listener.onSend(command);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void notifyConnect()
    {
        synchronized (listeners)
        {
            for (final ServerListener listener : listeners)
            {
                try
                {
                    listener.onConnect(server);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void notifyDisconnect()
    {
        synchronized (listeners)
        {
            for (final ServerListener listener : listeners)
            {
                try
                {
                    listener.onDisconnect(server);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}