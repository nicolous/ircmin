/**
 * 
 */
package com.ircmin.irc.network.api;

import java.net.InetSocketAddress;

/**
 * @author Nicolas Maingot
 *
 */
public interface ServerListener
{
    void onConnect(InetSocketAddress server);

    void onDisconnect(InetSocketAddress server);

    void onReceive(String data);

    void onSend(String data);
}
