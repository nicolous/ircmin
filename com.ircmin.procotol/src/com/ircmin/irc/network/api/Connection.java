/**
 * 
 */
package com.ircmin.irc.network.api;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * @author Nicolas Maingot
 *
 */
public interface Connection
{
    void connect() throws UnknownHostException, IOException;

    void disconnect() throws IOException;

    InetSocketAddress getServerAddress();

    int getPort();

    void send(String command) throws IOException;

    void addListener(ServerListener listener);

    void removeListener(ServerListener listener);
}
