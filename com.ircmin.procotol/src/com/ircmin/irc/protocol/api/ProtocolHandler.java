/**
 * 
 */
package com.ircmin.irc.protocol.api;

import java.io.IOException;
import java.util.Map;


/**
 * @author Nicolas Maingot
 *
 */
public interface ProtocolHandler
{
    String END_OF_CMD = "\r\n";

    void pass(String password) throws IOException;

    void nick(String nickname) throws IOException;

    void user(String username, String realname) throws IOException;

    void pong(String daemon) throws IOException;

    void quit() throws IOException;

    void quit(String message) throws IOException;

    void join(Map<String, String> channels) throws IOException;

    void part(String channel) throws IOException;

    void part(String channel, String message) throws IOException;

    void notice(String recipient, String message) throws IOException;

    void privmsg(String recipient, String message) throws IOException;

    void addListener(ProtocolListener listener);

    void removeListener(ProtocolListener listener);
}
