/**
 * 
 */
package com.ircmin.irc.protocol.api;

/**
 * @author Nicolas Maingot
 *
 */
public final class ErrorReplies
{
    /**
     * 
     */
    private ErrorReplies()
    {
    }

    public static final int START = 400;
    public static final int END = 599;

    /**
     * "&lt;nickname&gt; :No such nick/channel".
     */
    public static final int ERR_NOSUCHNICK = 401;

    /**
     * "&lt;server name&gt; :No such server".
     */
    public static final int ERR_NOSUCHSERVER = 402;

    /**
     * "&lt;channel name&gt; :No such channel".
     */
    public static final int ERR_NOSUCHCHANNEL = 403;

    /**
     * "&lt;channel name&gt; :Cannot send to channel".
     */
    public static final int ERR_CANNOTSENDTOCHAN = 404;

    /**
     * "&lt;channel name&gt; :You have joined too many channels".
     */
    public static final int ERR_TOOMANYCHANNELS = 405;

    /**
     * "&lt;nickname&gt; :There was no such nickname".
     */
    public static final int ERR_WASNOSUCHNICK = 406;

    /**
     * "&lt;target&gt; :&lt;error code&gt; recipients. &lt;abort message&gt;".
     */
    public static final int ERR_TOOMANYTARGETS = 407;

    /**
     * "&lt;service name&gt; :No such service".
     */
    public static final int ERR_NOSUCHSERVICE = 408;

    /**
     * ":No origin specified".
     */
    public static final int ERR_NOORIGIN = 409;

    /**
     * ":No recipient given (&lt;command&gt;)".
     */
    public static final int ERR_NORECIPIENT = 411;

    /**
     * ":No text to send".
     */
    public static final int ERR_NOTEXTTOSEND = 412;

    /**
     * "&lt;mask&gt; :No toplevel domain specified".
     */
    public static final int ERR_NOTOPLEVEL = 413;

    /**
     * "&lt;mask&gt; :Wildcard in toplevel domain".
     */
    public static final int ERR_WILDTOPLEVEL = 414;

    /**
     * "&lt;mask&gt; :Bad Server/host mask".
     */
    public static final int ERR_BADMASK = 415;

    /**
     * "&lt;command&gt; :Unknown command".
     */
    public static final int ERR_UNKNOWNCOMMAND = 421;

    /**
     * ":MOTD File is missing".
     */
    public static final int ERR_NOMOTD = 422;

    /**
     * "&lt;server&gt; :No administrative info available".
     */
    public static final int ERR_NOADMININFO = 423;

    /**
     * ":File error doing &lt;file op&gt; on &lt;file&gt;".
     */
    public static final int ERR_FILEERROR = 424;

    /**
     * ":No nickname given".
     */
    public static final int ERR_NONICKNAMEGIVEN = 431;

    /**
     * "&lt;nick&gt; :Erroneous nickname".
     */
    public static final int ERR_ERRONEUSNICKNAME = 432;

    /**
     * "&lt;nick&gt; :Nickname is already in use".
     */
    public static final int ERR_NICKNAMEINUSE = 433;

    /**
     * "&lt;nick&gt; :Nickname collision KILL from &lt;user&gt;@&lt;host&gt;".
     */
    public static final int ERR_NICKCOLLISION = 436;

    /**
     * "&lt;nick/channel&gt; :Nick/channel is temporarily unavailable".
     */
    public static final int ERR_UNAVAILRESOURCE = 437;

    /**
     * "&lt;nick&gt; &lt;channel&gt; :They aren't on that channel".
     */
    public static final int ERR_USERNOTINCHANNEL = 441;

    /**
     * "&lt;channel&gt; :You're not on that channel".
     */
    public static final int ERR_NOTONCHANNEL = 442;

    /**
     * "&lt;user&gt; &lt;channel&gt; :is already on channel".
     */
    public static final int ERR_USERONCHANNEL = 443;

    /**
     * "&lt;user&gt; :User not logged in".
     */
    public static final int ERR_NOLOGIN = 444;

    /**
     * ":SUMMON has been disabled".
     */
    public static final int ERR_SUMMONDISABLED = 445;

    /**
     * ":USERS has been disabled".
     */
    public static final int ERR_USERSDISABLED = 446;

    /**
     * ":You have not registered".
     */
    public static final int ERR_NOTREGISTERED = 451;

    /**
     * "&lt;command&gt; :Not enough parameters".
     */
    public static final int ERR_NEEDMOREPARAMS = 461;

    /**
     * ":Unauthorized command (already registered)".
     */
    public static final int ERR_ALREADYREGISTRED = 462;

    /**
     * ":Your host isn't among the privileged".
     */
    public static final int ERR_NOPERMFORHOST = 463;

    /**
     * ":Password incorrect".
     */
    public static final int ERR_PASSWDMISMATCH = 464;

    /**
     * ":You are banned from this server".
     */
    public static final int ERR_YOUREBANNEDCREEP = 465;

    /**
     * Sent by a server to a user to inform that access to the server will soon be denied.
     */
    public static final int ERR_YOUWILLBEBANNED = 466;

    /**
     * "&lt;channel&gt; :Channel key already set".
     */
    public static final int ERR_KEYSET = 467;

    /**
     * "&lt;channel&gt; :Cannot join channel (+l)".
     */
    public static final int ERR_CHANNELISFULL = 471;

    /**
     * "&lt;char&gt; :is unknown mode char to me for &lt;channel&gt;".
     */
    public static final int ERR_UNKNOWNMODE = 472;

    /**
     * "&lt;channel&gt; :Cannot join channel (+i)".
     */
    public static final int ERR_INVITEONLYCHAN = 473;

    /**
     * "&lt;channel&gt; :Cannot join channel (+b)".
     */
    public static final int ERR_BANNEDFROMCHAN = 474;

    /**
     * "&lt;channel&gt; :Cannot join channel (+k)".
     */
    public static final int ERR_BADCHANNELKEY = 475;

    /**
     * "&lt;channel&gt; :Bad Channel Mask".
     */
    public static final int ERR_BADCHANMASK = 476;

    /**
     * "&lt;channel&gt; :Channel doesn't support modes".
     */
    public static final int ERR_NOCHANMODES = 477;

    /**
     * "&lt;channel&gt; &lt;char&gt; :Channel list is full".
     */
    public static final int ERR_BANLISTFULL = 478;

    /**
     * ":Permission Denied- You're not an IRC operator".
     */
    public static final int ERR_NOPRIVILEGES = 481;

    /**
     * "&lt;channel&gt; :You're not channel operator".
     */
    public static final int ERR_CHANOPRIVSNEEDED = 482;

    /**
     * ":You can't kill a server!".
     */
    public static final int ERR_CANTKILLSERVER = 483;

    /**
     * ":Your connection is restricted!".
     */
    public static final int ERR_RESTRICTED = 484;

    /**
     * ":You're not the original channel operator".
     */
    public static final int ERR_UNIQOPPRIVSNEEDED = 485;

    /**
     * ":No O-lines for your host".
     */
    public static final int ERR_NOOPERHOST = 491;

    /**
     * ":Unknown MODE flag".
     */
    public static final int ERR_UMODEUNKNOWNFLAG = 501;

    /**
     * ":Cannot change mode for other users".
     */
    public static final int ERR_USERSDONTMATCH = 502;
}
