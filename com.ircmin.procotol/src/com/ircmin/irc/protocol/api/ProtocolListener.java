/**
 * 
 */
package com.ircmin.irc.protocol.api;

/**
 * @author Nicolas Maingot
 *
 */
public interface ProtocolListener
{
    void onPing(String daemon);

    void onReply(String sender, int code, String recipient, String message);

    void onNotice(String sender, String recipient, String message);

    void onPrivmsg(String sender, String recipient, String message);

    void onMode(String sender, String nickname, String mode);

    void onJoin(String sender, String channel);

    void onPart(String sender, String channel, String message);

    void onQuit(String sender, String message);

}
