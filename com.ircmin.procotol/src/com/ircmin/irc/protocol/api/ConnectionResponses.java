/**
 * 
 */
package com.ircmin.irc.protocol.api;

/**
 * @author Nicolas Maingot
 *
 */
public class ConnectionResponses
{
    /**
     * 
     */
    private ConnectionResponses()
    {
    }

    public static final int MIN = 0;
    public static final int MAX = 99;

    /* upon successful registration */

    /**
     * "Welcome to the Internet Relay Network &lt;nick&gt;!&lt;user&gt;@&lt;host&gt;".
     */
    public static final int RPL_WELCOME = 1;

    /**
     * "Your host is &lt;servername&gt;, running version &lt;ver&gt;".
     */
    public static final int RPL_YOURHOST = 2;

    /**
     * "This server was created &lt;date&gt;".
     */
    public static final int RPL_CREATED = 3;

    /**
     * "&lt;servername&gt; &lt;version&gt; &lt;available user modes&gt; &lt;available channel modes&gt;"
     * .
     */
    public static final int RPL_MYINFO = 4;

    /* connection is refused */

    /**
     * "Try server &lt;server name&gt;, port &lt;port number&gt;".
     */
    public static final int RPL_BOUNCE = 5;
}
