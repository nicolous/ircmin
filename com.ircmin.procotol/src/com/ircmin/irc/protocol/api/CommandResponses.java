/**
 * 
 */
package com.ircmin.irc.protocol.api;

/**
 * @author Nicolas Maingot
 *
 */
public final class CommandResponses
{
    /**
     * 
     */
    private CommandResponses()
    {
    }

    public static final int MIN = 200;
    public static final int MAX = 399;

    /**
     * "Link &lt;version & debug level&gt; &lt;destination&gt; &lt;next server&gt; V&lt;protocol
     * version&gt; &lt;link uptime in seconds&gt; &lt;backstream sendq&gt; &lt;upstream sendq&gt;".
     */
    public static final int RPL_TRACELINK = 200;

    /**
     * "Try. &lt;class&gt; &lt;server&gt;".
     */
    public static final int RPL_TRACECONNECTING = 201;

    /**
     * "H.S. &lt;class&gt; &lt;server&gt;".
     */
    public static final int RPL_TRACEHANDSHAKE = 202;

    /**
     * "???? &lt;class&gt; [&lt;client IP address in dot form&gt;]".
     */
    public static final int RPL_TRACEUNKNOWN = 203;

    /**
     * "Oper &lt;class&gt; &lt;nick&gt;".
     */
    public static final int RPL_TRACEOPERATOR = 204;

    /**
     * "User &lt;class&gt; &lt;nick&gt;".
     */
    public static final int RPL_TRACEUSER = 205;

    /**
     * "Serv &lt;class&gt; &lt;int&gt;S &lt;int&gt;C &lt;server&gt; &lt;nick!user|*!*&gt;@&lt;host|server&gt; V&lt;protocol version&gt;"
     * .
     */
    public static final int RPL_TRACESERVER = 206;

    /**
     * "Service &lt;class&gt; &lt;name&gt; &lt;type&gt; &lt;active type&gt;".
     */
    public static final int RPL_TRACESERVICE = 207;

    /**
     * "&lt;newtype&gt; 0 &lt;client name&gt;".
     */
    public static final int RPL_TRACENEWTYPE = 208;

    /**
     * "Class &lt;class&gt; &lt;count&gt;".
     */
    public static final int RPL_TRACECLASS = 209;

    /**
     * Unused.
     */
    public static final int RPL_TRACERECONNECT = 210;

    /**
     * "&lt;linkname&gt; &lt;sendq&gt; &lt;sent messages&gt; &lt;sent Kbytes&gt; &lt;received
     * messages&gt; &lt;received Kbytes&gt; &lt;time open&gt;".
     */
    public static final int RPL_STATSLINKINFO = 211;

    /**
     * "&lt;command&gt; &lt;count&gt; &lt;byte count&gt; &lt;remote count&gt;".
     */
    public static final int RPL_STATSCOMMANDS = 212;

    /**
     * "&lt;stats letter&gt; :End of STATS report".
     */
    public static final int RPL_ENDOFSTATS = 219;

    /**
     * "&lt;user mode string&gt;"
     */
    public static final int RPL_UMODEIS = 221;

    /**
     * "&lt;name&gt; &lt;server&gt; &lt;mask&gt; &lt;type&gt; &lt;hopcount&gt; &lt;info&gt;".
     */
    public static final int RPL_SERVLIST = 234;

    /**
     * "&lt;mask&gt; &lt;type&gt; :End of service listing".
     */
    public static final int RPL_SERVLISTEND = 235;

    /**
     * ":Server Up %d days %d:%02d:%02d".
     */
    public static final int RPL_STATSUPTIME = 242;

    /**
     * "O &lt;hostmask&gt; * &lt;name&gt;".
     */
    public static final int RPL_STATSOLINE = 243;

    /**
     * ":There are &lt;integer&gt; users and &lt;integer&gt; services on &lt;integer&gt; servers".
     */
    public static final int RPL_LUSERCLIENT = 251;

    /**
     * "&lt;integer&gt; :operator(s) online".
     */
    public static final int RPL_LUSEROP = 252;

    /**
     * "&lt;integer&gt; :unknown connection(s)".
     */
    public static final int RPL_LUSERUNKNOWN = 253;

    /**
     * "&lt;integer&gt; :channels formed".
     */
    public static final int RPL_LUSERCHANNELS = 254;

    /**
     * ":I have &lt;integer&gt; clients and &lt;integer&gt; servers".
     */
    public static final int RPL_LUSERME = 255;

    /**
     * "&lt;server&gt; :Administrative info".
     */
    public static final int RPL_ADMINME = 256;

    /**
     * ":&lt;admin info&gt;".
     */
    public static final int RPL_ADMINLOC1 = 257;

    /**
     * ":&lt;admin info&gt;".
     */
    public static final int RPL_ADMINLOC2 = 258;

    /**
     * ":&lt;admin info&gt;".
     */
    public static final int RPL_ADMINEMAIL = 259;

    /**
     * "File &lt;logfile&gt; &lt;debug level&gt;".
     */
    public static final int RPL_TRACELOG = 261;

    /**
     * "&lt;server name&gt; &lt;version & debug level&gt; :End of TRACE".
     */
    public static final int RPL_TRACEEND = 262;

    /**
     * "&lt;command&gt; :Please wait a while and try again.".
     */
    public static final int RPL_TRYAGAIN = 263;

    /**
     * "&lt;nick&gt; :&lt;away message&gt;".
     */
    public static final int RPL_AWAY = 301;

    /**
     * ":*1&lt;reply&gt; *( " " &lt;reply&gt; )".
     */
    public static final int RPL_USERHOST = 302;

    /**
     * ":*1&lt;nick&gt; *( " " &lt;nick&gt; )".
     */
    public static final int RPL_ISON = 303;

    /**
     * ":You are no longer marked as being away".
     */
    public static final int RPL_UNAWAY = 305;

    /**
     * ":You have been marked as being away".
     */
    public static final int RPL_NOWAWAY = 306;

    /**
     * "&lt;nick&gt; &lt;user&gt; &lt;host&gt; * :&lt;real name&gt;".
     */
    public static final int RPL_WHOISUSER = 311;

    /**
     * "&lt;nick&gt; &lt;server&gt; :&lt;server info&gt;".
     */
    public static final int RPL_WHOISSERVER = 312;

    /**
     * "&lt;nick&gt; :is an IRC operator".
     */
    public static final int RPL_WHOISOPERATOR = 313;

    /**
     * "&lt;nick&gt; &lt;user&gt; &lt;host&gt; * :&lt;real name&gt;".
     */
    public static final int RPL_WHOWASUSER = 314;

    /**
     * "&lt;name&gt; :End of WHO list".
     */
    public static final int RPL_ENDOFWHO = 315;

    /**
     * "&lt;nick&gt; &lt;integer&gt; :seconds idle".
     */
    public static final int RPL_WHOISIDLE = 317;

    /**
     * "&lt;nick&gt; :End of WHOIS list".
     */
    public static final int RPL_ENDOFWHOIS = 318;

    /**
     * "&lt;nick&gt; :*( ( "@" / "+" ) &lt;channel&gt; " " )".
     */
    public static final int RPL_WHOISCHANNELS = 319;

    /**
     * Obsolete. Not used.
     */
    public static final int RPL_LISTSTART = 321;

    /**
     * "&lt;channel&gt; &lt;# visible&gt; :&lt;topic&gt;".
     */
    public static final int RPL_LIST = 322;

    /**
     * ":End of LIST".
     */
    public static final int RPL_LISTEND = 323;

    /**
     * "&lt;channel&gt; &lt;mode&gt; &lt;mode params&gt;".
     */
    public static final int RPL_CHANNELMODEIS = 324;

    /**
     * "&lt;channel&gt; &lt;nickname&gt;".
     */
    public static final int RPL_UNIQOPIS = 325;

    /**
     * "&lt;channel&gt; :No topic is set".
     */
    public static final int RPL_NOTOPIC = 331;

    /**
     * "&lt;channel&gt; :&lt;topic&gt;".
     */
    public static final int RPL_TOPIC = 332;

    /**
     * "&lt;channel&gt; &lt;nick&gt;".
     */
    public static final int RPL_INVITING = 341;

    /**
     * "&lt;user&gt; :Summoning user to IRC".
     */
    public static final int RPL_SUMMONING = 342;

    /**
     * "&lt;channel&gt; &lt;invitemask&gt;".
     */
    public static final int RPL_INVITELIST = 346;

    /**
     * "&lt;channel&gt; :End of channel invite list".
     */
    public static final int RPL_ENDOFINVITELIST = 347;

    /**
     * "&lt;channel&gt; &lt;exceptionmask&gt;".
     */
    public static final int RPL_EXCEPTLIST = 348;

    /**
     * "&lt;channel&gt; :End of channel exception list".
     */
    public static final int RPL_ENDOFEXCEPTLIST = 349;

    /**
     * "&lt;version&gt;.&lt;debuglevel&gt; &lt;server&gt; :&lt;comments&gt;".
     */
    public static final int RPL_VERSION = 351;

    /**
     * "&lt;channel&gt; &lt;user&gt; &lt;host&gt; &lt;server&gt; &lt;nick&gt; ( "H" / "G" &gt; ["*
     * "] [ ( "@" / "+" ) ] :&lt;hopcount&gt; &lt;real name&gt;".
     */
    public static final int RPL_WHOREPLY = 352;

    /**
     * "( "=" / "*" / "@" ) &lt;channel&gt; :[ "@" / "+" ] &lt;nick&gt; *( " " [ "@" / "+" ]
     * &lt;nick&gt; ).
     */
    public static final int RPL_NAMREPLY = 353;

    /**
     * "&lt;mask&gt; &lt;server&gt; :&lt;hopcount&gt; &lt;server info&gt;".
     */
    public static final int RPL_LINKS = 364;

    /**
     * "&lt;mask&gt; :End of LINKS list".
     */
    public static final int RPL_ENDOFLINKS = 365;

    /**
     * "&lt;channel&gt; :End of NAMES list".
     */
    public static final int RPL_ENDOFNAMES = 366;

    /**
     * "&lt;channel&gt; &lt;banmask&gt;".
     */
    public static final int RPL_BANLIST = 367;

    /**
     * "&lt;channel&gt; :End of channel ban list".
     */
    public static final int RPL_ENDOFBANLIST = 368;

    /**
     * "&lt;nick&gt; :End of WHOWAS".
     */
    public static final int RPL_ENDOFWHOWAS = 369;

    /**
     * ":&lt;string&gt;".
     */
    public static final int RPL_INFO = 371;

    /**
     * ":- &lt;text&gt;".
     */
    public static final int RPL_MOTD = 372;

    /**
     * ":End of INFO list".
     */
    public static final int RPL_ENDOFINFO = 374;

    /**
     * ":- &lt;server&gt; Message of the day - ".
     */
    public static final int RPL_MOTDSTART = 375;

    /**
     * ":End of MOTD command".
     */
    public static final int RPL_ENDOFMOTD = 376;

    /**
     * ":You are now an IRC operator".
     */
    public static final int RPL_YOUREOPER = 381;

    /**
     * "&lt;config file&gt; :Rehashing".
     */
    public static final int RPL_REHASHING = 382;

    /**
     * "You are service &lt;servicename&gt;".
     */
    public static final int RPL_YOURESERVICE = 383;

    /**
     * "&lt;server&gt; :&lt;string showing server's local time&gt;".
     */
    public static final int RPL_TIME = 391;

    /**
     * ":UserID   Terminal  Host".
     */
    public static final int RPL_USERSSTART = 392;

    /**
     * ":&lt;username&gt; &lt;ttyline&gt; &lt;hostname&gt;".
     */
    public static final int RPL_USERS = 393;

    /**
     * ":End of users".
     */
    public static final int RPL_ENDOFUSERS = 394;

    /**
     * ":Nobody logged in".
     */
    public static final int RPL_NOUSERS = 395;
}
