/**
 * 
 */
package com.ircmin.irc.protocol;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ircmin.irc.network.api.Connection;
import com.ircmin.irc.network.api.ServerListener;
import com.ircmin.irc.protocol.api.ProtocolHandler;
import com.ircmin.irc.protocol.api.ProtocolListener;

/**
 * @author Nicolas Maingot
 *
 */
public class ProtocolHandlerImpl implements ProtocolHandler, ServerListener
{

    private final Connection connection;

    private final Set<ProtocolListener> listeners;

    /**
     * 
     */
    public ProtocolHandlerImpl(final Connection connection)
    {
        listeners = new HashSet<>();
        this.connection = connection;
        this.connection.addListener(this);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pass(final String password) throws IOException
    {
        connection.send("PASS " + password + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nick(final String nickname) throws IOException
    {
        connection.send("NICK " + nickname + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void user(
            final String username, final String realname) throws IOException
            {
        connection.send("USER " + username + " * * :" + realname + END_OF_CMD);
            }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void pong(final String daemon) throws IOException
    {
        connection.send("PONG " + daemon + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit() throws IOException
    {
        connection.send("QUIT" + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit(final String message) throws IOException
    {
        connection.send("QUIT :" + message + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void join(final Map<String, String> channels) throws IOException
    {
        if (!channels.isEmpty())
        {
            final StringBuilder buffer = new StringBuilder("JOIN ");
            for (final String chan : channels.keySet())
            {
                buffer.append(chan).append(',');
            }
            buffer.deleteCharAt(buffer.length() - 1);

            buffer.append(' ');

            boolean hasPassword = false;
            for (final String pass : channels.values())
            {
                if (pass != null)
                {
                    buffer.append(pass).append(',');
                    hasPassword = true;
                }
            }
            if (hasPassword)
                buffer.deleteCharAt(buffer.length() - 1);

            buffer.append(END_OF_CMD);

            connection.send(buffer.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void part(final String channel) throws IOException
    {
        connection.send("PART :" + channel + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void part(final String channel, final String message) throws IOException
    {
        connection.send("PART " + channel + " :" + message + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void notice(final String recipient, final String message) throws IOException
    {
        connection.send("NOTICE " + recipient + " :" + message + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void privmsg(final String recipient, final String message) throws IOException
    {
        connection.send("PRIVMSG " + recipient + " :" + message + END_OF_CMD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(final String data)
    {
        final String[] tokens = data.split(" ");

        final String firstToken = tokens[0];
        switch (firstToken)
        {
        case "PING":
            if (tokens.length >= 2)
            {
                final String daemon = tokens[1];
                try
                {
                    pong(daemon);
                    notifyPing(daemon);
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                }
            }
            break;

        default:
            if (firstToken.startsWith(":"))
            {
                final String sender = firstToken.substring(1);
                final String secondToken = tokens[1];

                switch (secondToken)
                {
                case "NOTICE":
                    if (tokens.length >= 4)
                    {
                        final String recip = tokens[2];
                        final String message =
                                data.substring(firstToken.length()
                                        + 1 + secondToken.length() + 1 + recip.length() + 2);
                        notifyNotice(sender, recip, message);
                    }
                    break;

                case "JOIN":
                    if (tokens.length >= 3)
                    {
                        final String channel = tokens[2];
                        notifyJoin(channel, sender);
                    }
                    break;

                case "PART":
                    if (tokens.length == 3)
                    {
                        final String channel = tokens[2].substring(1); // strip ":"
                        notifyPart(sender, channel, null);
                    }
                    else if (tokens.length > 3)
                    {
                        final String channel = tokens[2];
                        final String message =
                                data.substring(firstToken.length()
                                        + 1 + secondToken.length() + 1 + channel.length() + 2);
                        notifyPart(sender, channel, message);
                    }
                    break;

                case "QUIT":
                    if (tokens.length == 3)
                    {
                        final String channel = tokens[2].substring(1); // strip ":"
                        notifyPart(sender, channel, null);
                    }
                    else if (tokens.length > 3)
                    {
                        final String message =
                                data.substring(firstToken.length() + 1 + secondToken.length() + 2);
                        notifyQuit(sender, message);
                    }
                    break;

                case "PRIVMSG":
                    if (tokens.length >= 2)
                    {
                        final String recip = tokens[2];
                        final String message =
                                data.substring(firstToken.length()
                                        + 1 + secondToken.length() + 1 + recip.length() + 2);
                        notifyPrivmsg(sender, recip, message);
                    }
                    break;

                case "MODE":
                    if (tokens.length >= 2)
                    {
                        final String nickname = tokens[2];
                        final String mode = tokens[3].substring(1); // strip ":"
                        notifyMode(sender, nickname, mode);
                    }
                    break;

                default:
                    if (secondToken.length() == 3
                    && Character.isDigit(secondToken.charAt(0))
                    && Character.isDigit(secondToken.charAt(1))
                    && Character.isDigit(secondToken.charAt(2)) && tokens.length > 2)
                    {
                        final String recip = tokens[2];
                        final String message =
                                data.substring(firstToken.length()
                                        + 1 + secondToken.length() + 1 + recip.length() + 1);
                        notifyReply(sender, Integer.parseInt(secondToken), recip, message);
                    }
                    else
                    {
                        System.err.println(data);
                    }
                    break;
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSend(final String data)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnect(final InetSocketAddress server)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisconnect(final InetSocketAddress server)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(final ProtocolListener listener)
    {
        synchronized (listeners)
        {
            listeners.add(listener);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeListener(final ProtocolListener listener)
    {
        synchronized (listeners)
        {
            listeners.remove(listener);
        }
    }

    private void notifyPing(final String daemon)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onPing(daemon);
        }
    }

    private void notifyReply(
            final String sender, final int code, final String recipient, final String message)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onReply(sender, code, recipient, message);
        }
    }

    private void notifyNotice(final String sender, final String recipient, final String message)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onNotice(sender, recipient, message);
        }
    }

    private void notifyJoin(final String channel, final String sender)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onJoin(channel, sender);
        }
    }

    private void notifyPart(final String sender, final String channel, final String message)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onPart(sender, channel, message);
        }
    }

    private void notifyQuit(final String sender, final String message)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onQuit(sender, message);
        }
    }

    private void notifyPrivmsg(final String sender, final String recipient, final String message)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onPrivmsg(sender, recipient, message);
        }
    }

    private void notifyMode(final String sender, final String nickname, final String mode)
    {
        synchronized (listeners)
        {
            for (final ProtocolListener listener : listeners)
                listener.onMode(sender, nickname, mode);
        }
    }
}
